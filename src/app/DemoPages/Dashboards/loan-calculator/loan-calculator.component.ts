import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Options } from 'ng5-slider';
import { Color, Label, SingleDataSet } from 'ng2-charts';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-loan-calculator',
  templateUrl: './loan-calculator.component.html',
  styleUrls: ['./loan-calculator.component.scss']
})
export class LoanCalculatorComponent implements OnInit {

  datasetChanged = new Subject();
  barDatasetChanged = new Subject();

  heading = 'LOAN CALCULATOR';
  subheading = 'This is an example dashboard created using build-in elements and components.';
  icon = 'pe-7s-plane icon-gradient bg-tempting-azure';

  allMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


// Slider options

  loanAmountValue: number = 1000;
  loanAmountOptions: Options = {
    floor: 1000,
    ceil: 500000,
    step: 10000,
    tickStep: 100000,
    showTicksValues: true,
    showSelectionBar: true,
  }

  interestRateValue: number = 5;
  interestRateOptions: Options = {
    floor: 0,
    ceil: 20,
    step: 0.25,
    tickStep: 5,
    showTicksValues: true,
    showSelectionBar: true,
  }
  
  loanTerm: string = 'month';
  loanTermValue: number = 12;

  floor = 0;
  ceil = 480;
  step = 12;
  tickStep = 120;

  loanTermOptions: Options = {
    floor: this.floor,
    ceil: this.ceil,
    step: this.step,
    tickStep: this.tickStep,
    showTicksValues: true,
    showSelectionBar: true,
  }

  // Amortization

  paymentData: any = []; //  Full payments
  principalData: any = []; //  Principal payments
  interestData: any = [];  //  Interest payments
  balanceData: any = []; // Balance left to be paid

  months: any = [];
  
  hidden: any = [true]; //  Hidden collapsables

  // Chart options

  private ctx = <HTMLCanvasElement> document.getElementById('chart');


  public doughnutChartLabels: Label[] = ['Principal Amount', 'Total Interest'];
  public doughnutChartData: SingleDataSet = [ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartColors: Color[] = [
    { 
      backgroundColor: ['blue', 'orange'],
      borderColor: ['black', 'orange'],
      borderWidth: 7.5,
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ];



  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {xAxes: [{}], yAxes: [{}]},
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['2019', '2020'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  // public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    {data: this.principalData, label: 'Principal Amount'},
    {data: this.interestData, label: 'Interest Amount'}
  ];
  

  // Date

  date: Date = new Date();
  month: number = this.date.getMonth();
  monthlyPayment: number = Math.round(this.interestRateValue*this.loanAmountValue/100 + this.loanAmountValue)/this.loanTermValue;
  totalPayment: number = this.interestRateValue*this.loanAmountValue/100 + this.loanAmountValue;

  // FORM

  form: FormGroup;



  // CSS options

  @ViewChild('dateDiv') dateDiv: ElementRef;
  @ViewChild('chartDiv') chartDiv: ElementRef;
  @ViewChild('tableDiv') tableDiv: ElementRef;



  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      age: new FormControl(null, [Validators.required, this.numberValidator]),
      phone: new FormControl(null, [Validators.required, this.numberValidator]),
      email: new FormControl(null, [Validators.required, Validators.email]),

    });

    this.getBarchartData();

    for(let i=0; i<(12-this.date.getMonth()); i++) {
      this.months.push(i);
    }

    this.datasetChanged.subscribe(
      (data) => {
        this.doughnutChartData = [data];
        this.barChartData = [
          {data: this.principalData, label: 'Principal Amount'},
          {data: this.interestData, label: 'Interest Amount'}
        ];
      }
    );

  }

  public incrementLoanAmountValue(): void {
    this.loanAmountValue = this.loanAmountValue+10000;
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }

  public decrementLoanAmountValue(): void {
    this.loanAmountValue = this.loanAmountValue-10000;
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }

  public incrementInterestRateValue(): void {
    this.interestRateValue = this.interestRateValue+0.25;
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }
  
  public decrementInterestRateValue(): void {
    this.interestRateValue = this.interestRateValue-0.25;
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }

  public incrementLoanTermValue(): void {
    if(this.loanTerm == 'month') {
      this.loanTermValue = this.loanTermValue+12;
      this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
    } else {
      this.loanTermValue = this.loanTermValue+1;
      this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
    }
  }

  public decrementLoanTermValue(): void {
    if(this.loanTerm == 'month') {
      this.loanTermValue = this.loanTermValue-12;
      this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
    } else {
      this.loanTermValue = this.loanTermValue-1;
      this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
    }
  }

  public onRadioChange(event: any): void {
    this.loanTerm = event.srcElement.value;
    if(this.loanTerm == 'month') {
      this.loanTermOptions = {
        floor: 0,
        ceil : 480,
        step : 12,
        tickStep : 120,
        showTicksValues: true,
        showSelectionBar: true,
      }
    } else {
      this.loanTermOptions = {
        floor : 0,
        ceil : 40,
        step : 1,
        tickStep : 10,
        showTicksValues: true,
        showSelectionBar: true,
      }
    }
  }

  public onLoanSliderChange(): void {
    this.getBarchartData();
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }

  public onInterestSliderChange(): void {
    this.getBarchartData();
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }

  public onLoanTermSliderChange(): void {
    this.getBarchartData();
    this.datasetChanged.next([ this.loanAmountValue, (this.interestRateValue*this.loanAmountValue+this.loanTermValue)/(100) ]);
  }
  

  public getBarchartData(): void {
    let paymentData: any = []; //  Full payments
    let principalData: any = []; //  Principal payments
    let interestData: any = [];  //  Interest payments
    let balanceData: any = []; // Balance left to be paid
    let years: number;  //  Number of years selected
    let labels: any = [];
    let hidden: boolean[] = [];
    let months: number;
    
    if(this.loanTerm == 'month') {
      years = this.loanTermValue/12;
      months = this.loanTermValue;
    } else {
      years = this.loanTermValue;
    }
    for(let i = 0; i < years; i++) {
      labels.push(this.date.getFullYear()+i);
      hidden.push(true);

      let yearsPayment = 0;
      if(i != 0) {
      // For every year but the first

        principalData.push(12*this.monthlyPayment*(100-this.interestRateValue)/100);
        interestData.push(12*this.monthlyPayment*(this.interestRateValue)/100);
        yearsPayment = 12*this.monthlyPayment;
        paymentData.push(yearsPayment);
        balanceData.push(this.totalPayment-yearsPayment);

      } else {
      // For first year we get the price from starter month

        principalData.push((12-this.month)*this.monthlyPayment*(100-this.interestRateValue)/100);
        interestData.push((12-this.month)*this.monthlyPayment*(this.interestRateValue)/100);
        yearsPayment = (12-this.month)*this.monthlyPayment;
        paymentData.push(yearsPayment);
        balanceData.push(this.totalPayment-yearsPayment);

      }
    }
    this.barChartLabels = labels;
    this.hidden = hidden;
    this.paymentData = paymentData;
    this.principalData = principalData;
    this.interestData = interestData;
    this.balanceData = balanceData;
  }

  public onTableRow(index: number): void {
    this.hidden[index] = !this.hidden[index];
    console.log(this.hidden[index], this.hidden);
  }

  public onDateChange(event): void {
    this.date = event.srcElement.valueAsDate;
    this.months = [];
    for(let i=0; i<(12-this.date.getMonth()); i++) {
      this.months.push(i);
    }
    this.datasetChanged.next();
  }

  public numberValidator(control: any): {[key: string]: any} | null {
    const valid = /^\d+$/.test(control.value);
    return valid ? null : { nan: { valid: false, value: control.value }};
  }

  public handleScrollEvent(element: any): void {
    const {x, y} = element.getBoundingClientRect();
    if(y < window.innerHeight) {
      this.animateDateDiv();
    }
  }

  public handleChartScroll(element: any): void {
    const {x, y} = element.getBoundingClientRect();
    if(y < window.innerHeight) {
      this.animateChartDiv();
    }
  }

  public handleTableScroll(element: any): void {
    const {x, y} = element.getBoundingClientRect();
    if(y < window.innerHeight) {
      this.animateTableDiv();
    }
  }

  public animateDateDiv(): void {
    this.dateDiv.nativeElement.className = 'col-12 fadeIn';
  }

  public animateChartDiv(): void {
    this.chartDiv.nativeElement.className = 'col-12 zoomIn';
  }

  public animateTableDiv(): void {
    this.tableDiv.nativeElement.className = 'col-12 fadeIn';
  }
  
}
